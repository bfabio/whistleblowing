(function form() {
  var crypto = new Crypto();
  var formdata = new FormData();

  function getKey(filename) {
    $.get(filename + '.asc', function(data) {
       window[filename] = openpgp.key.readArmored(data).keys;
    });
  }

  getKey('pubkey_whistle');
  getKey('pubkey_user');

  $('input[type=file]').change(function(event) {
    var files = event.target.files

    for (var i = 0; i < files.length; i++) {
      var file = files[i];
      var reader = new FileReader();

      reader.onload = (function(file) {
        return function(event) {
          //console.log(file.name, 'base64', event.target.result);
          var msg = openpgp.message.fromText(event.target.result);
          msg = msg.encrypt(window.pubkey_whistle);
          var armored = openpgp.armor.encode(openpgp.enums.armor.message, msg.packets.write());
          var blob = crypto.byteStringToBlob(armored);

          formdata.append('files', blob, file.name);
          document.getElementById('uploadFile').value += file.name + ' ';
        };
      })(file);

      reader.readAsDataURL(file);
    }
  });

  $('form').submit(function (e) {
    var user = crypto.serializeInputs('div.user :input');
    var msg = crypto.serializeInputs('div.msg :input');

    Promise.all([
      crypto.encryptMsg(JSON.stringify(msg), window.pubkey_whistle),
      crypto.encryptMsg(JSON.stringify(user), window.pubkey_user)
    ]).then(function(values) {
      formdata.append('msg', values[0]);
      formdata.append('user', values[1]);
      formdata.append('email', $('#email').val());

      $.ajax({
        type: 'POST',
        url: 'save',
        data: formdata,
        cache: false,
        processData: false,
        contentType: false
      })
      .success(function(response) {
        document.open();
        document.write(response);
        document.close();
      })
      .error(function(err) {
        console.log(err);
      });
    });

    e.preventDefault();
    return false;
  });
})();
