var Crypto = function() {
  this.privkey = null;
  this.pubkey = null;
}

Crypto.prototype = {
  byteStringToBlob: function(byteString) {
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);

    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ab]);
  },

  base64toBlob: function(dataURI) {
    var byteString = atob(dataURI.split(',')[1]);
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

    return this.byteStringToBlob(byteString);
  },

  decryptKey: function(key, passphrase) {
    var decryptkey = openpgp.key.readArmored(key).keys[0];
    decryptkey.decrypt(passphrase);
    return decryptkey;
  },

  decryptPrivKey: function(key, passphrase) {
    this.privkey = this.decryptKey(key, passphrase);
    return this.privkey;
  },

  decryptPubKey: function(key, passphrase) {
    this.pubkey = this.decryptKey(key, passphrase);
    return this.pubkey;
  },

  decryptMsg: function(msg, privkey) {
    privkey = privkey || this.privkey;
    msg = openpgp.message.readArmored(msg);

    return openpgp.decryptMessage(privkey, msg);
  },

  encryptMsg: function(msg, pubkey) {
    pubkey = pubkey || this.pubkey;

    return openpgp
      .encryptMessage(pubkey, msg)
      .then(function(plaintext) {
        return plaintext;
      }).catch(function(error) {
        console.error(error);
      });
  },

  serializeInputs: function(selector) {
    var obj = {};

    $(selector).each(function(index, input) {
      input = $(input);
      obj[$('label[for="' + input.attr('id') + '"]').text()] = input.val();
    });

    return obj;
  }
};
